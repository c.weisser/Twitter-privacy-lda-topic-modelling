import glob

from preprocess import *
import pickle
import pandas as pd
from time import time

path = "data/"


start_time = time()
#print("Show first five files")
#print(glob.glob("D://twitter_data/data/Indonesia/*")[0:5])

print("reading in data from:", path)
json_data= read_datafiles(path)
print("reading data complete")

print("start parsing")
json_data = parse_tweets(json_data)
print("parsing complete")

print("read stopwords")
indo_dict = pd.read_csv("Survey result_Twitter Data.csv")
covid_keywords = indo_dict.loc[indo_dict["Category/Topic"] == "covid",["Indonesia"]].values.flatten()
sex_keywords = indo_dict.loc[indo_dict["Category/Topic"] == "sex",["Indonesia"]].values.flatten()
print("covid", len(covid_keywords),"sex", len(sex_keywords))

print("filter for covid")
json_data = filter_tweets(json_data, covid_keywords)
print(len(json_data),"kept")

print("exclude prostitution")
json_data = filter_tweets(json_data, sex_keywords, include=False)
print(len(json_data),"kept")

print("export data")
pickle.dump( json_data, open( "Indonesia_covid.pkl", "wb" ) )
print("data packer done! time elapes:", round(time()-start_time), "seconds")